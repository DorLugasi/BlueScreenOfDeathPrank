using Microsoft.Win32;
class clsTaskManager
{
    public static void ToggleTaskManager()
    {
        RegistryKey objRegistryKey = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
        if (objRegistryKey.GetValue("DisableTaskMgr") == null)
        {
            objRegistryKey.SetValue("DisableTaskMgr", "1", RegistryValueKind.DWord);
        }
        else
        {
            objRegistryKey.DeleteValue("DisableTaskMgr");
        }
        objRegistryKey.Close(); 
    }

    public static void DisableTaskManager()
    {           
        RegistryKey objRegistryKey = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
        objRegistryKey.SetValue("DisableTaskMgr", "1");
        objRegistryKey.Close();
    }
    public static void ReleaseTaskManager()
    {
        RegistryKey objRegistryKey = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
        if (objRegistryKey.GetValue("DisableTaskMgr") != null)
        {
            objRegistryKey.DeleteValue("DisableTaskMgr");
        }
    }
}