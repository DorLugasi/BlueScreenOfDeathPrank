﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlueScreenOfDeathPrank
{
    public partial class frmInit : Form
    {
        public static KeyboardHook kh;
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        public frmInit()
        {
            //EnableStuff();
            //return;

            Screen[] screensArray = Screen.AllScreens;
            DisableStuff();
            foreach (var currentScreen in screensArray)
            {
                frmMain frmMain = new frmMain();
                frmMain.Show();
                frmMain.Height = currentScreen.Bounds.Height;
                frmMain.Width = currentScreen.Bounds.Width;
            }
            InitializeComponent();
            Thread.Sleep(5000);
            EnableStuff();
            foreach (var process in Process.GetProcessesByName("BlueScreenOfDeathPrank"))
            {
                process.Kill();
            }
        }
        public void DisableStuff()
        {
            clsTaskbar.Hide();
            clsTaskManager.DisableTaskManager();
            Cursor.Hide();
            VolumeChanger.Mute();
            kh = new KeyboardHook();
        }
        public void EnableStuff()
        {
            clsTaskbar.Show();
            clsTaskManager.ReleaseTaskManager();
            Cursor.Show();
            VolumeChanger.Mute();
            kh.Dispose();
        }
    }
}
